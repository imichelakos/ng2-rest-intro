## RESTful Angular 2 application - Intro -

## Dependencies
- You must have `node v >= 4.0` and `npm` installed
- `npm i -g typings webpack-dev-server webpack rimraf`

### Getting Started

To get started run the commands below.

```bash
$ git clone https://gitlab.com/imichelakos/ng2-rest-intro.git
$ cd ng2-rest-intro
$ npm install -g json-server
$ npm install
$ npm start
```

Then navigate to [http://localhost:3001](http://localhost:3001) in your browser.