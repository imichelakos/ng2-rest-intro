import {Component} from 'angular2/core';
import {Http, Response} from 'angular2/http';

@Component({
    selector: 'basic',
    template: `
    <h2>Basic Request</h2>
    <button class="mdl-button mdl-raised mdl-js-button mdl-js-ripple-effect"  (click)="makeRequest()">Make Request</button>
    <div *ngIf="loading">Loading...</div>
    <pre>{{data | json}}</pre>
    `

})
export class Basic {

    data: Object;
    loading: boolean;

    constructor(public http: Http){

    }

    makeRequest(): void{
        this.loading = true;
        this.http.request('http://jsonplaceholder.typicode.com/posts/1')
            .subscribe((res: Response) => {
                this.data = res.json();
                this.loading = false;

            });
    }

}